﻿DROP DATABASE IF EXISTS b20190605;
CREATE DATABASE IF NOT EXISTS b20190605;

USE b20190605;

CREATE OR REPLACE TABLE personas(
  dni varchar(10),
  nombre varchar(50),
  PRIMARY KEY(dni)
);

INSERT INTO personas (dni,nombre)
  VALUES ('d1','nombre1'),
         ('d2','nombre2'),
         ('d3','nombre3');

CREATE OR REPLACE TABLE telefonos(
  dni varchar(10),
  numero varchar(12),
  PRIMARY KEY(dni,numero),
  CONSTRAINT fkTelefonosPersonas FOREIGN KEY (dni)
    REFERENCES personas(dni) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO telefonos (dni, numero)
  VALUES ('d1','t1'),
         ('d1','t2'),
         ('d2','t3'),
         ('d3','t4');