﻿DROP DATABASE IF EXISTS b20190606;
CREATE DATABASE IF NOT EXISTS b20190606;

-- Multivaluado

USE b20190606;

CREATE OR REPLACE TABLE socios(
  codigo int,
  nombre varchar(50),
  PRIMARY KEY (codigo)
);

CREATE OR REPLACE TABLE correos(
  socio int,
  email varchar(50),
  PRIMARY KEY(socio,email),
  CONSTRAINT fkEmailsSocios FOREIGN KEY (socio)
    REFERENCES socios(codigo) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE OR REPLACE TABLE telefonos(
  socio int,
  telefono varchar(14),  
  PRIMARY KEY(socio,telefono),
  CONSTRAINT fkTelefonosSocios FOREIGN KEY (socio)
    REFERENCES socios(codigo) ON DELETE CASCADE on UPDATE CASCADE
);

CREATE OR REPLACE TABLE peliculas(
  codigo int,
  titulo varchar(10),
  PRIMARY KEY(codigo)
);

CREATE OR REPLACE TABLE alquilan(
  socio int,
  pelicula int,
  PRIMARY KEY(socio,pelicula),
  CONSTRAINT fkAlquilanSocios FOREIGN KEY (socio)
    REFERENCES socios(codigo) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fkAlquilanPeliculas FOREIGN KEY (pelicula)
    REFERENCES peliculas(codigo) ON DELETE CASCADE ON UPDATE CASCADE
); 

-- Multivalor fecha en la relacion alquilan (se recoge la primary los dos campos)
CREATE OR REPLACE TABLE fechas(
  socio int,
  pelicula int,
  fecha date,
  PRIMARY KEY(socio,pelicula,fecha),
  CONSTRAINT fkFechasAlquilan FOREIGN KEY (socio,pelicula)
    REFERENCES alquilan(socio,pelicula) ON DELETE CASCADE ON UPDATE CASCADE
);